/*
  header TRysFADC.h for
  class TRysFADC.cpp to plot
    primitive histograms from FADC

  J.Szabelski 20/06/2005
  2/2/2013
*/

#ifndef JS_PLOT_FADC_5
#define JS_PLOT_FADC_5

#include <stdio.h>
#include <string.h>
#include <iostream>

#include "TROOT.h"
#include "TCanvas.h"
#include "TH1S.h"
//#include "TAxis.h"
#include "TLine.h"
#include "TList.h"
#include "TMath.h"
#include "TText.h"
#include "TMarker.h"

#include "TFadcData.h"
//#include "TCzytPikiTree.h"
#include "TReference.h"


/* 0000000000000000000000000000000000000000000 */
  class TRysFADC
    : public TObject
 {
  private:
// TCzytPikiTree *pikiROOT;
// TEventPiki *evPiki;
   TReference *ref;

   Int_t retPiki;

   TFadcData   *F[8];
   TLine       *lPix0[8];
   TLine       *lPixPick[N_FADC_K][N_IMP_L];
   TText       *ttADC, *ttPoz;
   TMarker     *mMaxADC;
   TCanvas     *c8;

   TCanvas*  cSel;
   TH1S*     hSel;

   Int_t lFADC;                // number of FADC pixels recorded
   Int_t trFADC;               //pixel corresponding to trigger time
   Int_t nanosec_per_pixel;    // number of nanosecons per pixel
   Int_t nrPrzypadku;          // event number to be draw
   Char_t data_czas[32];         // czas rejestracji znakowo
   Char_t torWybrany;         // wybrany nr toru (jesli przypadek byl "Selected")
   Float_t adcMax;          // max ADC (jesli przypadek byl "Selected")
   Int_t adcMaxPoz;         // polozenie max ADC (jesli przypadek byl "Selected")

   void Zeros(void);

  public:
   TH1S        *hhFadc[8];
   TPad        *padFADC[8];

//      TRysFADC(TCzytPikiTree *qpikiROOT);
        TRysFADC(TReference *qref);
       ~TRysFADC(void);

   Int_t Draw8H(TList *listaFADC);

   Int_t DrawSelected(const Int_t i1, const Int_t i2);   // tworzy i rysuje wybrany histogram
                      // w wybranym zakresie pikseli

   void SetEventNumber(const Int_t qnrPrzypadku) {nrPrzypadku = qnrPrzypadku;};
   void SetTorWybrany(const Char_t qtorWybrany) {torWybrany = qtorWybrany;};
   void SetADCmax(const Float_t qadcMax) {adcMax = qadcMax;};
   void SetADCmaxPoz(const Int_t qadcMaxPoz) {adcMaxPoz=qadcMaxPoz;};
   void SetASCIItime(Char_t *qdata_czas) {strcpy(data_czas, qdata_czas);};
   Int_t GetEventNumber(void) {return nrPrzypadku;};
   TCanvas *GetOstatniCanvas(void) {return c8;};
   void OpisyOsi(const Int_t i, TH1S *h);
   void LiniaTriggera(const Int_t i, TVirtualPad *pad);
   void LiniePikow(const Int_t i, TVirtualPad *pad);
   void InfoPisana(const Int_t i, TVirtualPad *pad);

/* =========================================== */
   public:
   ClassDef(TRysFADC,0);
 };

#endif


