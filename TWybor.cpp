/*
  class/structure TWybor.cpp zawierajacy informacje
   dotyczaca opracowania wynikow
   w tym polozenia poczatkow pikow

  J.Szabelski 28/12/2012
*/

#include"TWybor.h"

ClassImp(TWybor);

// **********************
TWybor::TWybor(TReference *qref)
//TWybor::TWybor(TSelected *qSel, TApplication *qTAp, FILE *qwybrane, const Int_t qNoOfN)
{
    ref = qref;

    rob = new TPrzemial(ref);

    Sel = ref->Sel;
    TAp = ref->TAp;
    NoOfN = ref->NoOfN;
    wybrane = ref->wybrane;

    iOstatni = -1;  // nr poprzedniego wyswietlanego przebiegu
    iSelected = -1;  // nr wybranego przypadku w klasie TSelected
    kSelected = 0;   // licznik wybranych

}

TWybor::~TWybor(void)
{
    if (rob) {delete rob; rob = NULL;}
}

// ^^^^^^^^^^^^^^^^^^^^^^

// **********************
//Int_t TWybor::WyborGuzikiem(const Int_t iAktualny, TEvent *qEvnt)
Int_t TWybor::WyborGuzikiem(const Int_t iAktualny)
{
    Evnt = ref->Evnt;
    Int_t nGuzik;
    kNowy = -999;
    Int_t rret = -1;
    Sel->ResetK();

    // selekcja nastepnego przypadku
    G = new TGGuzikiJS(nGuzik,TAp);

    iWybrany = G->GetNumer();  // numer przypadku, gdy nGuzik=6
    iTriggChannel = G->GetTriggerCh();  // kanal triggera, gdy nGuzik=8
    iPulseNo = G->GetPulseNo();    // liczba pulsow, gdy nGuzik=8

    delete G; G=NULL;

    switch (nGuzik) {

    case 1:    // wybrany nastepny
        kNowy = Sel->GetNastepny(iAktualny, iSelected);
        if (kNowy < 0) {
            rret = 4;
            break;
        }
        kSelected++;
        iOstatni = iAktualny;
        rret =  2;  // petlaW
        break;

    case 2:     // wybrany poprzedni
        kNowy = Sel->GetPoprzedni(iAktualny, iSelected);
        if (kNowy < 0) {
            kNowy = Sel->GetSelEventNo(0); // pierwszy wybrany nWybrany[0]
        }
        kSelected++;
        iOstatni = iAktualny;
        rret =  2;  // petlaW
        break;

    case 3:     // jeszcze raz ten sam
        kNowy = iAktualny;
        rret =  2;  // petlaW
        break;

    case 4:     // nastepny bez selekcji
        kNowy = iAktualny + 1;
        if (kNowy >= NoOfN) { kNowy = iAktualny;}
        else {iOstatni = iAktualny;}
        rret =  2;  // petlaW
        break;

    case 5:     // poprzedni bez selekcji
        kNowy = iAktualny - 1;
        if (kNowy >= NoOfN) { kNowy = iAktualny;}
        else {iOstatni = iAktualny;}
        rret =  2;  // petlaW
        break;

    case 6:     // wybrany numer
        if ((iWybrany<0) || (iWybrany>=NoOfN)) {
            kNowy = iAktualny;
        }
        else { kNowy = iWybrany;}
        rret =  2;  // petlaW
        break;

    case 7:   // quit
        rret = 4;
        break;

    case 8:   // select
        Event_Analiza_Zapis(iAktualny);
        rret = 2;
        break;


    default:   // ten sam
        kNowy = iAktualny;
        rret =  2;  // petlaW
        break;
    }

    return rret;
}
// ^^^^^^^^^^^^^^^^^^^^^^

// **********************
Int_t TWybor::Event_Analiza_Zapis(const Int_t iAktualny)
{
    /*
  Char_t ai;
  Int_t n, bi, di;
  Float_t ci;
  n=iAktualny;
  ai = Sel->GetSelTor(n);
  ai= Int_t (ai);
  bi= 0;//bi=i3, ?
  ci = Sel->GetSelADCmax(n);
  di = Sel-> GetSelMaxPoz(n);
*/
    printf("Zapis do pliku.\n");
    rob->UsunPiki();
    rob->Poziom0_50_100();    // "pedestals"
    rob->MaxADCzPolozeniem();  // max i polozenia w przypadku

    fprintf(wybrane, "%5d %10d", iAktualny, Evnt->t70);
    fprintf(wybrane, " %1d %2d", iTriggChannel, iPulseNo);
    for (Int_t i=0; i<8; i++) {
        fprintf(wybrane, "  %3d %3d", (Int_t)(ref->Poziom0_50_100[i]+0.5) , (Int_t)ref->ucMaxADC[i]);
    }
    fprintf(wybrane, "\n");

    kNowy = iAktualny;

    return 0;
}


