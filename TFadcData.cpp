/*
  class/structure TFadcData.cpp to keep one FADC for one EAS event data
   (Lodz EAS array)
  (for ROOT or C++)

  J.Szabelski 15/1/2003, 14/12/2003, 1/1/2016
*/

#include "TROOT.h"
#include"TFadcData.h"

   ClassImp(TFadcData);

// **********************
  TFadcData::TFadcData(void)
 {
  Reset();
 }

  TFadcData::~TFadcData(void){}
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
//void TFadcData::CopyTo(TFadcData *G)
// {
//Int_t i;
//UChar_t *c, *d;
//c = G->FADC; d=FADC; for (i=0;i<N_FADC_0;i++) {*c++ = *d++;}
// }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  void TFadcData::CopyTo(TFadcData *G, Int_t i0, Int_t i1)
 {
  Int_t i;
  UChar_t *c, *d;
  c = G->FADC; d=FADC; for (i=i0;i<i1;i++) {*c++ = *d++;}
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  void TFadcData::FromArray2FADC(UChar_t *a)
 {
  Int_t i;
  UChar_t *c, *d;
  c=FADC; d = a; for (i=0;i<N_FADC_0;i++) {*c++ = *d++;}
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  void TFadcData::Reset(void)
 {
  Int_t i;
  UChar_t *c;
  c = FADC; for (i=0;i<N_FADC_0;i++) {*c++ = (UChar_t)0;}
 }
// ^^^^^^^^^^^^^^^^^^^^^

