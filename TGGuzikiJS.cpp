/*
  class TGGuzikiJS.cpp to make
    TGButton's table for decission

  K.Jedrzejczak & J.Szabelski 4/2/2003
  J.Szabelski 16/5/2005 
  J.Szabelski 11/1/2016 , 23/3/2016
*/

#include"TGGuzikiJS.h"

  ClassImp(TGGuzikiJS);

/*******************************
  constructor
-----------------------------*/
  TGGuzikiJS::TGGuzikiJS(Int_t &qkNacisniety, TApplication *qtheAppl)
 {
  theAppl = qtheAppl;
  Bny = NULL;
  kNacisniety = 0;
  bNumerOK = kFALSE;
  Char_t *c; c=cKtoryNastepny; for (Int_t i=0;i<8;i++){*c++=(Char_t)0;}

  Wodotrysk();
  qkNacisniety = kNacisniety;

 }
/* ^^^^^^^^^^^^^^^^^ */
/*******************************
  destructor
-----------------------------*/
 TGGuzikiJS::~TGGuzikiJS(void)
 {
  if (Bny) {delete Bny; Bny = NULL;}
 }

/* ^^^^^^^^^^^^^^^^^ */

/*******************************
  Wodotrysk 
-----------------------------*/
void  TGGuzikiJS::Wodotrysk(void)
{
jeszczeRaz:
     TGWindow *p;
     p = (TGWindow *)gClient->GetRoot();
     Bny = new TGOdpowiedz04(p, 280, 360, theAppl);
     theAppl->Run(kTRUE);

     kNacisniety = Bny->GetNacisniety();
     if (kNacisniety==6) {  // przycisk: numer przypadku
         strcpy(cKtoryNastepny, Bny->GetNumberString());
     }
     else if (kNacisniety==8) {  // przycisk: select
         kNumerKanaluTriggera = Bny->GetTrigChannel();
         kLiczbaPulsow = Bny->GetPulseNumber();
     }

     delete Bny; Bny = NULL;

     if (kNacisniety==6) {  // numer przypadku - zamiana char --> liczbe
         bNumerOK = CzyToLiczba(cKtoryNastepny);
         if (bNumerOK) {kNumerNastepnegoPrzypadku = ChIconv(cKtoryNastepny);}
         if (!bNumerOK) {goto jeszczeRaz;} //character nie jest liczba
     }
 }
 /* ^^^^^^^^^^^^^^^^^ */

 /*******************************
  Sprawdzenie, czy string jest liczba
-----------------------------*/
   Bool_t TGGuzikiJS::CzyToLiczba(Char_t *cc)
 {
  Bool_t bb = kTRUE; 
  Int_t l = strlen(cc);
  if (l) {
     Char_t *c; c=cc;
     for (Int_t i=0;i<l;i++) {
       if ((*c) != ' ') {
          if ((*c) < '0') { bb = kFALSE;}
          if ((*c) > '9') { bb = kFALSE;}
         }
        c++;
       }
    }
   else {
     bb = kFALSE;
    }
  return bb;
 }
/* ^^^^^^^^^^^^^^^^^ */

/*******************************
  Sprawdzenie, czy string jest liczba
-----------------------------*/
   Int_t TGGuzikiJS::ChIconv(Char_t *cc)
 {
  Int_t number;
  Int_t k = sscanf(cc,"%d",&number); 
  if (k!= 1) {number = -1; bNumerOK = kFALSE;}
  return number;
 }
/* ^^^^^^^^^^^^^^^^^ */
