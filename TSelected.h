/*
  header TSelected.h for
  class/structure TSelected.cpp to keep one FADC for one EAS event data
   (Lodz EAS array)

  J.Szabelski 30/11/2012
*/

#ifndef JS_SELECTED_
#define JS_SELECTED_

#include "stdio.h"
#include "TROOT.h"

#define N_SELECTED 12288

/* 0000000000000000000000000000000000000000000 */
  class TSelected
 {
  private:
   Int_t status;
   FILE *p;
   Int_t nSel; // liczba wybranych wczytanych z pliku

   Int_t kSel; // ostatnio "Wybrany"
   Int_t nWybrane[N_SELECTED]; //wybrane eventy (numery przypadkow)
   Char_t torWybrany[N_SELECTED]; // nr toru wybranego przypadku
// Float_t adcMax[N_SELECTED]; // max amplituda (srednia z kilku)
// Int_t adcMaxPoz[N_SELECTED]; // polozenie max adc (nr pixela) 

   Int_t CzytajSelected(void);

  public:

        TSelected(Char_t *name);
       ~TSelected(void);

   Int_t GetNastepny(const Int_t k, Int_t &ixN);  // nastepny wybrany po k-tym, ixN - index w nWybranych
   Int_t GetPoprzedni(const Int_t k, Int_t &ixN);
   Int_t GetStatus(void) {return status;}
   Int_t GetNSel(void){return nSel;}
   Bool_t CzyWybrany(const Int_t k, Int_t &ixN);
   void ResetK(void) {kSel = -1;}  // "zerowanie" kSel
   Int_t   GetKSel(void){return kSel;}
   Int_t   GetSelEventNo(const Int_t n) {return nWybrane[n];}
   Char_t  GetSelTor(const Int_t n) {return torWybrany[n];}
// Float_t GetSelADCmax(const Int_t n) {return adcMax[n];};
// Int_t   GetSelMaxPoz(const Int_t n) {return adcMaxPoz[n];};

/* =========================================== */
   public:
   ClassDef(TSelected,0);
 };

#endif


