/*
  class TGOdpowiedz04.cpp to make
    Canvas for decission buttons
   and returns kNacisniety - nr nacisnietego guzika
        liczac od 1 od gory

 based on TGOdpowiedz01 by
  K.Jedrzejczak & J.Szabelski 4/2/2003
  J.Szabelski 16/5/2005

  by:
  K. Wojczuk 22/10/2016, 21/3/2016
  J. Szabelski 11/1/2016, 23/3/2016
  M. Kasztelan 12/04/2016
*/

#include "TGOdpowiedz04.h"

ClassImp(TGOdpowiedz04);

/*******************************
  constructor
-----------------------------*/
TGOdpowiedz04::TGOdpowiedz04(const TGWindow *p, UInt_t w, UInt_t h, TApplication *qtheAppl)
    :TGMainFrame(p, w, h)
{
    theAppl = qtheAppl;

    Char_t *c; c=cKtoryNastepny; for (Int_t i=0;i<8;i++){*c++=(Char_t)0;}
    kRadioButtomChannel = -2;
    kRadioButtomPulseNo = -2;

    fVert = new TGVerticalFrame(this, 60, 70);
    //fVert = new TGVerticalFrame(this,160,20);

    fLayout = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY);  // kLHintsCenterX and kLHintsCenterY
    // from TGLayout.h:
    //  kLHintsCenterX = 2  = 0x02
    //  kLHintsCenterY = 16 = 0x10
    //  kLHintsCenterX | kLHintsCenterY = 18 = 0x12
    fLayoutNumer = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 2,2,2,2 );
    // JS: nie wiem, o co chodzi ?

    Int_t i;
    i = 2;
    fHorGuzikiSelect = new TGHorizontalFrame(fVert,50,20,4|8);
    fGuzikPreviousSelected = new TGTextButton(fHorGuzikiSelect, "Previous selected", i);
    fGuzikPreviousSelected->Associate(this);
    fHorGuzikiSelect->AddFrame(fGuzikPreviousSelected, fLayoutNumer);

    i = 1;
    fGuzikNextSelected = new TGTextButton(fHorGuzikiSelect, "Next selected", i);
    fGuzikNextSelected->Associate(this);
    fHorGuzikiSelect->AddFrame(fGuzikNextSelected, fLayoutNumer);
//    fVert->AddFrame(fGuzikNextSelected, fLayout);

    fVert->AddFrame(fHorGuzikiSelect, fLayout);  // Dodajemy ramke z 2 przyciskami do pionowej ramki globalnej.
    i = 3;
    fGuzikThisOne = new TGTextButton(fVert, "&Once again", i);
    fGuzikThisOne->Associate(this);
    fVert->AddFrame(fGuzikThisOne, fLayout);

    i = 5;
    fHorGuzikiEvent = new TGHorizontalFrame(fVert,50,20,4|8);
    fGuzikPrevious = new TGTextButton(fHorGuzikiEvent, "&Previous event", i);
    fGuzikPrevious->Associate(this);
    fHorGuzikiEvent->AddFrame(fGuzikPrevious, fLayoutNumer);

    i = 4;
    fGuzikNext = new TGTextButton(fHorGuzikiEvent, "&Next event", i);
    fGuzikNext->Associate(this);
    Pixel_t kolor_zielony;
    gClient->GetColorByName("green", kolor_zielony);
    fGuzikNext->SetForegroundColor(kolor_zielony);
//    fGuzikNext->Connect("Clicked()", "TGOdpowiedz04", this, "DoSaveActivate()");
    fHorGuzikiEvent->AddFrame(fGuzikNext, fLayoutNumer);

    fVert->AddFrame(fHorGuzikiEvent, fLayoutNumer);  // Dodajemy ramke z 2 przyciskami do pionowej ramki globalnej.

    i = 6;
    fHorNumb = new TGHorizontalFrame(fVert,50,20,4|8);
    //fHorNumb = new TGHorizontalFrame(fVert,80,10,4|8);   // bez roznicy

    fGuzikGiveNumber = new TGTextButton(fHorNumb, "Give event number:", i);
    fGuzikGiveNumber->Associate(this);
    fHorNumb->AddFrame(fGuzikGiveNumber, fLayoutNumer);


    fTekst = new TGTextBuffer(64);
    fWpisLiczbe = new TGTextEntry(fHorNumb,fTekst);
    fWpisLiczbe->Resize(50, fWpisLiczbe->GetDefaultHeight());
    fHorNumb->AddFrame(fWpisLiczbe,fLayoutNumer);

    fVert->AddFrame(fHorNumb,fLayout);

    i = 7;
    fGuzikQuit = new TGTextButton(fVert, "&quit", i);
    fGuzikQuit->Associate(this);
    fVert->AddFrame(fGuzikQuit, fLayout);

    i = 8; //mod
    fLayoutSelect = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 2,2,2,2 );


    // JS (24/3/2016):
    // MK (29/3/2016):
    // ------------------------------- trigger channel:
    fRadioChanelSelect = new TGHorizontalFrame(fVert, 100, 50, 4| 8);  // Tylko na RadioButtonGroup
    fLayoutRadioChSelect = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 8, 3, 2, 2);

    fRadioGrCh = new TGButtonGroup(fRadioChanelSelect, 0, 8, 0, 0, "trigger channel");
    for (Int_t k=0; k<kNbOfChannels; k++)
    {
        fRadioOpisVertFrame[k] = new TGVerticalFrame(fRadioGrCh, 15, 25);  // Ramka pionowa na (Opis i Radio Button)  x 8
        Char_t chR[2];
        sprintf(chR, "%1d", k);
        fRadioChannelLabels[k] = new TGLabel(fRadioOpisVertFrame[k], chR);
        fRadioCh[k] = new TGRadioButton(fRadioOpisVertFrame[k], "", 100+k);

        fRadioOpisVertFrame[k]->AddFrame(fRadioChannelLabels[k], fLayoutRadioChSelect );
        fRadioOpisVertFrame[k]->AddFrame(fRadioCh[k], fLayoutRadioChSelect);

        fRadioGrCh->AddFrame(fRadioOpisVertFrame[k], fLayoutSelect);
        fRadioCh[k]->Connect("Clicked()", "TGOdpowiedz04", this, "DoRadioChannel()");  // Ta funkcja obsługuje ten RadioButton
    }
    fRadioChanelSelect->AddFrame(fRadioGrCh, fLayoutRadioChSelect);
    fVert->AddFrame(fRadioChanelSelect, fLayout);

    // ------------------------------- nunber of pulses:
    fRadioPulseNoSelect = new TGHorizontalFrame(fVert, 100, 20, 4| 8);
    fLayoutRadioPulseNoSelect = new TGLayoutHints(kLHintsCenterX | kLHintsCenterY, 8,3,2,2 );

    fRadioGrPulseNo = new TGButtonGroup(fRadioPulseNoSelect, 0, kMaxNbOfSignals, 0, 0, "no of pulses");
    for (Int_t k=0; k<kMaxNbOfSignals; k++)
    {
        fRadioOpisPulseVertFrame[k] = new TGVerticalFrame(fRadioGrPulseNo, 15, 25);  // Ramka pionowa na (Opis i Radio Button)  x 8
        Char_t chR[2];
        sprintf(chR, "%1d", k);
        fRadioPulseNoLabels[k] = new TGLabel(fRadioOpisPulseVertFrame[k], chR);
        fRadioPulseNo[k] = new TGRadioButton(fRadioOpisPulseVertFrame[k], "", 200+k);

        fRadioOpisPulseVertFrame[k]->AddFrame(fRadioPulseNoLabels[k], fLayoutRadioPulseNoSelect);
        fRadioOpisPulseVertFrame[k]->AddFrame(fRadioPulseNo[k], fLayoutRadioPulseNoSelect);
        fRadioGrPulseNo->AddFrame(fRadioOpisPulseVertFrame[k],  fLayoutSelect);
        fRadioPulseNo[k]->Connect("Clicked()", "TGOdpowiedz04", this, "DoRadioNoPulse()");  // Ta funkcja obsługuje ten RadioButton
    }
//    fRadioGrPulseNo->SetButton(201);
    fRadioPulseNo[1]->SetDown();    // domyslnie 1 puls
    kRadioButtomPulseNo = 1;        // domyslnie 1 puls
    fRadioPulseNoSelect->AddFrame(fRadioGrPulseNo, fLayoutRadioPulseNoSelect);
    fVert->AddFrame(fRadioPulseNoSelect, fLayout);
    // -------------------------------

    //--------------------------------------   Potwierdzenie identyfikacji przypadku.
    fHorSelect = new TGHorizontalFrame(fVert,50,20,4| 8);
    //Select trigger channel then total number of pulses
    fGuzikSelect = new TGTextButton(fHorSelect, "&SAVE  select event: trigger channel & NoOfPulses:", i);
    fGuzikSelect->Associate(this);
    fGuzikSelect->SetEnabled(kFALSE);   // Domyslnie wylaczony przycik. Najpierw trzeba wybrac numer kanalu z triggerem.
    fHorSelect->AddFrame(fGuzikSelect, fLayoutSelect);
    fVert->AddFrame(fHorSelect, fLayout);
    //--------------------------------------

    AddFrame(fVert,fLayout);

    MapSubwindows();
    Layout();
    SetWindowName("Wodotrysk");
    SetIconName("wybory");
    MapWindow();

    TCanvas *fCanvasFADC = (TCanvas*)gROOT->GetListOfCanvases()->FindObject("canvas8H");
    UInt_t width_canvas_8H = fCanvasFADC->GetWindowWidth();
    UInt_t topX_canvas8H = fCanvasFADC->GetWindowTopX();

    Move(width_canvas_8H+topX_canvas8H,0);
}
/* ^^^^^^^^^^^^^^^^^ */
/*******************************
  destructor
-----------------------------*/
TGOdpowiedz04::~TGOdpowiedz04(void)
{
    for (Int_t i = 0; i<kMaxNbOfSignals; i++) {
        if (fRadioPulseNo[i])       {delete fRadioPulseNo[i]; fRadioPulseNo[i] = NULL;}
        if (fRadioOpisPulseVertFrame[i]) {delete fRadioOpisPulseVertFrame[i]; fRadioOpisPulseVertFrame[i] = NULL;}
        if (fRadioPulseNoLabels[i]) {delete fRadioPulseNoLabels[i]; fRadioPulseNoLabels[i] = NULL;}
    }
    if (fRadioGrPulseNo)            {delete fRadioGrPulseNo; fRadioGrPulseNo = NULL;}
    if (fLayoutRadioPulseNoSelect)  {delete fLayoutRadioPulseNoSelect; fLayoutRadioPulseNoSelect = NULL;}
    if (fRadioPulseNoSelect)        {delete fRadioPulseNoSelect; fRadioPulseNoSelect = NULL;}

    for (Int_t i = 0; i<kNbOfChannels; i++) {
        if (fRadioCh[i])            {delete fRadioCh[i]; fRadioCh[i] = NULL;}
        if (fRadioOpisVertFrame[i]) {delete fRadioOpisVertFrame[i]; fRadioOpisVertFrame[i] = NULL;}
        if (fRadioChannelLabels[i]) {delete fRadioChannelLabels[i]; fRadioChannelLabels[i] = NULL;}
    }
    if (fRadioGrCh)             {delete fRadioGrCh; fRadioGrCh = NULL;}
    if (fLayoutRadioChSelect)   {delete fLayoutRadioChSelect; fLayoutRadioChSelect = NULL;}
    if (fRadioChanelSelect)     {delete fRadioChanelSelect; fRadioChanelSelect = NULL;}

    if (fGuzikSelect)       {delete fGuzikSelect; fGuzikSelect=NULL;}//mod
    if (fLayoutSelect)      {delete fLayoutSelect; fLayoutSelect=NULL;}
    if (fHorSelect)         {delete fHorSelect; fHorSelect=NULL;}

    if (fGuzikQuit)         {delete fGuzikQuit; fGuzikQuit=NULL;}
    if (fWpisLiczbe)        {delete fWpisLiczbe; fWpisLiczbe=NULL;}
    if (fGuzikGiveNumber)   {delete fGuzikGiveNumber; fGuzikGiveNumber=NULL;}
    if (fLayoutNumer)       {delete fLayoutNumer; fLayoutNumer=NULL;}
    if (fHorNumb)           {delete fHorNumb; fHorNumb=NULL;}
    if (fGuzikPrevious)     {delete fGuzikPrevious; fGuzikPrevious=NULL;}
    if (fGuzikNext)         {delete fGuzikNext; fGuzikNext=NULL;}
    if (fGuzikThisOne)      {delete fGuzikThisOne; fGuzikThisOne=NULL;}
    if (fGuzikPreviousSelected) {delete fGuzikPreviousSelected; fGuzikPreviousSelected=NULL;}
    if (fGuzikNextSelected) {delete fGuzikNextSelected; fGuzikNextSelected=NULL;}

    if(fHorGuzikiSelect)    {delete fHorGuzikiSelect;   fHorGuzikiSelect=NULL;}
    if(fHorGuzikiEvent)     {delete fHorGuzikiEvent;    fHorGuzikiSelect=NULL;}
    if (fLayout)            {delete fLayout;            fLayout=NULL;}
    if (fVert)              {delete fVert;              fVert=NULL;}
}
/* ^^^^^^^^^^^^^^^^^ */
void TGOdpowiedz04::DoRadioChannel()
{
    TGButton *btn = (TGButton *) gTQSender;
    Int_t id = btn->WidgetId();

    if (id >= 100 && id <= 199)
    {
        for (int i = 0; i < kNbOfChannels; i++)
        {
            if (fRadioCh[i]->WidgetId() != id)
                fRadioCh[i]->SetState(kButtonUp);
            else
                kRadioButtomChannel = i;
            fGuzikSelect->SetEnabled(kTRUE);   // Aktywujemy przycisk umozliwiajac zapisanie wyniku.
        }
    }
}
void TGOdpowiedz04::DoRadioNoPulse()
{
    TGButton *btn = (TGButton *) gTQSender;
    Int_t id = btn->WidgetId();

    if (id >= 200 && id <= 299)
    {
        for (int i = 0; i < kMaxNbOfSignals; i++)
        {
            if (fRadioPulseNo[i]->WidgetId() != id)
                fRadioPulseNo[i]->SetState(kButtonUp);
            else
                kRadioButtomPulseNo = i;
        }
    }
}
void TGOdpowiedz04::DoSaveActivate() // unused
{
    // MK: Narazie testowo. Nie uzywana funkcja.
    Printf("DoSaveActivate");
    Pixel_t kolor_zielony;
    gClient->GetColorByName("green", kolor_zielony);
    fGuzikNext->SetForegroundColor(kolor_zielony);
}
/*******************************
  Wodotrysk
-----------------------------*/
Bool_t TGOdpowiedz04::ProcessMessage(Long_t msg, Long_t parm1, Long_t )
{
    // Process events generated by the buttons in the frame.
    switch (GET_MSG(msg))
    {
    case kC_COMMAND:
        switch (GET_SUBMSG(msg))
        {
        case kCM_BUTTON:
            kNacisniety = (Int_t)parm1;
            if (parm1 == 6) { // numer przypadku
                strcpy(cKtoryNastepny, fTekst->GetString());
            }
//            else
//                if (parm1 == 8) { // select : trigger chanel and pulse no

//                kRadioButtomChannel = -1;
//                for (Int_t i=0; i<8; i++) {
//                    if (fRadioCh[i]->IsDown()) { kRadioButtomChannel = i; break;}
//                }

//                kRadioButtomPulseNo = -1;
//                for (Int_t i=0; i<5; i++) {
//                    if (fRadioPulseNo[i]->IsDown()) { kRadioButtomPulseNo = i; break;}
//                }
//                printf("TGOdpowiedz04::ProcessMessage: kRadioButtomChannel=%4d  kRadioButtomPulseNo=%4d\n",
//                       kRadioButtomChannel, kRadioButtomPulseNo);
//            }
            break;
        default:
            break;
        }
    default:
        break;
    }

    theAppl->Terminate(0);

    return kTRUE;
}
/* ^^^^^^^^^^^^^^^^^ */
