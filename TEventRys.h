/*
  header TEventRys.h for
  class / interface for ploting TEventRys.cpp

  J.Szabelski 16/1/2016
*/

#ifndef JS_EVNT_PLOT_02
#define JS_EVNT_PLOT_02

#include "TList.h"
#include "TPad.h"

#include "TEvent.h"
#include "TRysFADC.h"
//#include "TCzytPikiTree.h"
#include "TSelected.h"
#include "TReference.h"

/* 0000000000000000000000000000000000000000000 */
  class TEventRys
    : public TObject
 {
  private:
    TReference *ref;

    TEvent *Evnt;
    TRysFADC *Plot;
//  TCzytPikiTree *pikiROOT;
    TSelected *Sel;

    TPad *Pads[8];

    TList *ListFADC;
    Int_t nWLiscie;

  public:
    Char_t data_czas[32];


//      TEventRys(TList *qListFADC, TRysFADC *qPlot);
//      TEventRys(TList *qListFADC);
//      TEventRys(TList *qListFADCT, TCzytPikiTree *qpikiROOT, TSelected *qSel);
        TEventRys(TReference *qref);
       ~TEventRys(void);

// TRysFADC *makePlots(const Int_t i, TEvent *qEvnt);
   TRysFADC *makePlots(const Int_t i);
// Int_t makePlots(const Int_t i, TEvent *qEvnt);

/* =========================================== */
   public:
   ClassDef(TEventRys,0);
 };

#endif


