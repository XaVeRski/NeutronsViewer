/*
  class/structure TEvent.cpp to keep one event data

  J.Szabelski 14/1/2016
*/
#include "TROOT.h"
#include"TEvent.h"

   ClassImp(TEvent);

// **********************
  TEvent::TEvent(void)
 {
  for (Int_t i=0; i<N_KANAL; i++) {
    przeb2[i] = new TFadcData();
   }

  Reset();
 }

  TEvent::~TEvent(void)
 {
  for (Int_t i=0; i<N_KANAL; i++) {
    delete przeb2[i]; przeb2[i] = NULL;
   }
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  void TEvent::CopyTo(TEvent *G)
 {
  for (Int_t i=0; i<N_KANAL; i++) {przeb2[i]->CopyTo(G->przeb2[i]);}
  Char_t *c, *d;
  c = ch_czas; d = G->ch_czas;
  for (Int_t i=0; i<16; i++) {*d++ = *c++;}
  G->t70 = t70;
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
  void TEvent::Reset(void)
 {
  for (Int_t i=0; i<N_KANAL; i++) {przeb2[i]->Reset();}
  Char_t *c;
  c = ch_czas;
  for (Int_t i=0; i<16; i++) {*c++ = (Char_t)0;}
  t70 = static_cast<UInt_t>(0);
 }
// ^^^^^^^^^^^^^^^^^^^^^

