/*
  class / interface for ploting TEventRys.cpp

  J.Szabelski 16/1/2016
*/

#include"TEventRys.h"

   ClassImp(TEventRys);

// **********************
//TEventRys::TEventRys(TList *qListFADC, TRysFADC *qPlot)
//TEventRys::TEventRys(TList *qListFADC)
//TEventRys::TEventRys(TList *qListFADC, TCzytPikiTree *qpikiROOT, TSelected *qSel)
  TEventRys::TEventRys(TReference *qref)
 {
  ref = qref;
  ListFADC = ref->ListFADC;
  nWLiscie = ListFADC->GetEntries();
//Plot = qPlot;
//pikiROOT = ref->pikiROOT;
  Sel = ref->Sel;

  Plot = NULL;
 }


  TEventRys::~TEventRys(void)
 {
  if (Plot) {delete Plot; Plot = NULL;}
 }
// ^^^^^^^^^^^^^^^^^^^^^

// **********************
//TRysFADC *TEventRys::makePlots(const Int_t i, TEvent *qEvnt)
  TRysFADC *TEventRys::makePlots(const Int_t i)
//Int_t TEventRys::makePlots(const Int_t i, TEvent *qEvnt)
 {
  Evnt = ref->Evnt;

  sprintf(data_czas,"%c%c%c%c-%c%c-%c%c %c%c:%c%c:%c%c%c",
                  Evnt->ch_czas[0],Evnt->ch_czas[1],Evnt->ch_czas[2],Evnt->ch_czas[3],
                  Evnt->ch_czas[4],Evnt->ch_czas[5],Evnt->ch_czas[6],Evnt->ch_czas[7],
                  Evnt->ch_czas[8],Evnt->ch_czas[9],Evnt->ch_czas[10],Evnt->ch_czas[11],
                  Evnt->ch_czas[12],Evnt->ch_czas[13],
                  (Char_t)0);

  if (Plot) {delete Plot; Plot=NULL;}

  Plot = new TRysFADC(ref);
//ref->Plot = Plot;
  Plot->SetEventNumber(i);
  Plot->SetASCIItime(data_czas);
//kSel = Sel->GetKSel();
//if (kSel >=0) {
//if (Sel->CzyWybrany(i, kSel)) {

//  Plot->SetTorWybrany(Sel->GetSelTor(kSel));
//  Plot->SetADCmax(Sel->GetSelADCmax(kSel));
//  Plot->SetADCmaxPoz(Sel->GetSelMaxPoz(kSel));
// }
  Plot->Draw8H(ListFADC);
  Plot->DrawSelected(1400, 2600);

 // .....................
        // ustawianie procedury wyboru Pad do powiekszenia
  Char_t nazProc[16];
  Char_t nazWyw[32];

  sprintf(nazProc,"WybierzPad");
  for (Int_t j=0; j< nWLiscie; j++) {
//  printf("j=%d   Plot=%p\n", j, Plot);
    sprintf(nazWyw, "WybierzPad(%d)", j);
    Pads[j] = Plot->padFADC[j];
    Pads[j]->AddExec( nazProc, nazWyw);
   }
 // .....................

  return Plot;
 }

